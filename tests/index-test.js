var chai = require('chai');
var expect = chai.expect;
var tjbot = require('../index.js')();
var OPTS = require('../opts.js');

describe('Telegram Jira Bot', function() {
    var msg, match, issue, user, notvalidUser, notvalidIssue;
    before(function() {
        msg = {
            from: {
                id: 123, first_name: 'John'
            },
            chat: {
                id: 123, first_name: 'John', type: 'private'
            },
            text: '/user  John'};
        match = ['/user John', 'John'];
        issue = {key: 'ABC-123', fields: {summary: 'Some title', description: 'Some description'}};
        notvalidIssue = {fields: {description: null}};
        user = {
            displayName: 'John Doe',
            name: 'johndoe',
            emailAddress: 'johndoe@mail.com'
        };
        notvalidUser = {displayName: null, emailAddress: null};
    });
    describe('getters', function() {
        it('getUserId()', function() {
            expect(tjbot.getUserId(msg)).to.be.a('number');
        });
        it('getParam()', function() {
            expect(tjbot.getParam(match)).to.be.a('string');
        });
        it('getRequestUser()', function() {
            expect(tjbot.getRequestUser('John'))
                .to.be.an('object')
                .that.has.property('username')
                .that.is.a('string')
                .that.equals(encodeURIComponent('John'));
        });
        it('getQueryAssignee()', function() {
            expect(tjbot.getQueryAssignee('johndoe')).to.be.a('string');
            expect(tjbot.getQueryAssignee('johndoe')).to.equal("assignee = 'johndoe'");
        });
    });
    describe('templates strings', function() {
        it('createIssueTmpl()', function() {
            expect(tjbot.createIssueTmpl(issue)).to.be.a('string');
            expect(tjbot.createIssueTmpl(issue)).to.equal('https://'+OPTS.jira.host+'/browse/ABC-123\nSome title\n\n');
        });
        it('createUserTmpl()', function() {
            expect(tjbot.createUserTmpl(user)).to.be.a('string');
            expect(tjbot.createUserTmpl(user)).to.equal('John Doe\njohndoe\njohndoe@mail.com');
        });
        it('createTicketTmpl()', function() {
            expect(tjbot.createTicketTmpl(issue)).to.be.a('string');
            expect(tjbot.createTicketTmpl(issue)).to.equal('Some title\n\nSome description');
        });
    });
    describe('checks', function() {
        it('checkUser()', function() {
            expect(tjbot.checkUser(notvalidUser)).to.be.an('object');

            expect(tjbot.checkUser(notvalidUser))
                .to.have.property('displayName')
                .that.is.a('string')
                .that.equals('Имя не указано');

            expect(tjbot.checkUser(notvalidUser))
                .that.has.property('emailAddress')
                .that.is.a('string')
                .that.equals('Email не указан');

            expect(tjbot.checkUser(user))
                .to.have.property('displayName')
                .that.is.a('string')
                .that.equals('John Doe');

            expect(tjbot.checkUser(user))
                .to.have.property('emailAddress')
                .that.is.a('string')
                .that.equals('johndoe@mail.com');
        });
        it('checkTicket()', function() {
            expect(tjbot.checkTicket(notvalidIssue)).to.be.an('object');

            expect(tjbot.checkTicket(notvalidIssue))
                .to.have.deep.property('fields.description')
                .that.is.a('string')
                .that.equals('Нет описания');

            expect(tjbot.checkTicket(issue))
                .to.have.deep.property('fields.description')
                .that.is.a('string')
                .that.equals('Some description');
        });
    });
});