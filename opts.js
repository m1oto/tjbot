module.exports = {
    botToken: '',
    jira: {
        protocol: 'https',
        host: '',
        username: '',
        password: '',
        apiVersion: '2',
        strictSSL: true
    }
};