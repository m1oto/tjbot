var fs = require('fs');
var R = require('ramda');
var OPTS = require('./opts.js');
var JiraApi = require('jira-client');
var TelegramBot = require('node-telegram-bot-api');
var bot = new TelegramBot(OPTS.botToken, {polling: true});
var jira = new JiraApi(OPTS.jira);

var TJbot = function() {
	function INIT() {
		bot.onText(/\/issues (.+)/, issuesHandler);
		bot.onText(/\/ticket (.+)/, ticketHandler);
		bot.onText(/\/user (.+)/, userHandler);
	}
	/**
	 * Send message to user with current ID
	 * @param {string} msg
	 * @type {Function}
	 */
	const sendMsgToCurrentUser = R.compose(R.curry(sendMsgToUser), getUserId);
	/**
	 * Handler of '/user' command
	 * @param {string} msg
	 * @param {string} match
	 */
	function userHandler(msg, match) {
		jira.searchUsers(getRequestUser(getParam(match)))
			.then(R.map(checkUser))
			.then(R.map(createUserTmpl))
			.then(R.forEach(sendMsgToCurrentUser(msg)))
			.catch(errorHandler);
	}
	/**
	 * Handler of '/issues' command
	 * @param {string} msg
	 * @param {string} match
	 */
	function issuesHandler(msg, match) {
		jira.searchJira(getQueryAssignee(getParam(match)))
			.then(R.prop('issues'))
			.then(R.map(createIssueTmpl))
			.then(R.forEach(sendMsgToCurrentUser(msg)))
			.catch(errorHandler);
	}
	/**
	 * Handler of '/ticket' command
	 * @param {string} msg
	 * @param {string} match
	 */
	function ticketHandler(msg, match) {
		jira.findIssue(getParam(match))
			.then(errorHandler)
			.then(checkTicket)
			.then(createTicketTmpl)
			.then(sendMsgToCurrentUser(msg))
			.catch(errorHandler);
	}
	/**
	 * Send message to User
	 * @param {string} userId
	 * @param {*} msg
	 */
	function sendMsgToUser(userId, msg) {
		console.log(userId);
		console.log(msg);
		bot.sendMessage(userId, msg);
	}
	/**
	 * Getting User ID from message
	 * @param {object} msg
	 * @returns {number}
	 */
	function getUserId(msg) {
		return msg.from.id;
	}
	/**
	 * Getting param from message	function getParam(match) {

	 * @param match
	 * @returns {string}
	 */
	function getParam(match) {
		return match[1];
	}
	/**
	 * Create username object for search request
	 * @param {string} user
	 * @returns {{username: string}}
	 */
	function getRequestUser(user) {
		return {
			username: encodeURIComponent(user)
		};
	}
	/**
	 * Create user assigned search queury
	 * @param userName
	 * @returns {string} JQL formatted
	 */
	function getQueryAssignee(userName) {
		return ["assignee = '", userName, "'"].join('');
	}
	/**
	 * Create message with ticket to send
	 * @param {string} issue
	 * @returns {string} ticket
	 */
	function createTicketTmpl(issue) {
		return [issue.fields.summary, issue.fields.description].join('\n\n');
	}
	/**
	 * Create message with user to send
	 * @param {string} issue
	 * @returns {string} ticket
	 */
	function createUserTmpl(user) {
		return [user.displayName, user.name, user.emailAddress].join('\n');
	}
	/**
	 * Create message with issue to send
	 * @param {string} issue
	 * @returns {string} ticket
	 */
	function createIssueTmpl(val) {
		return ['https://' + OPTS.jira.host + '/browse/', val.key, '\n', val.fields.summary, '\n\n'].join('');
	}
	/**
	 * @param {string} msg
	 */
	function errorHandler(msg) {
		console.log(msg);
		return msg;
	}
	/**
	 * Validate ticket params
	 * @param {{fileds: {description: string}}} issue
	 * @returns {{fileds: {description: string}}}
     */
	function checkTicket(issue) {
		var issueClone = R.clone(issue);

		issueClone.fields.description = R.defaultTo('Нет описания', issue.fields.description);
		return issueClone;
	}

	/**
	 * Validate user params
	 * @param {displayName: string, emailAddress: string}} user
	 * @returns {displayName: string, emailAddress: string}}
     */
	function checkUser(user) {
		var userClone = R.clone(user);

		userClone.displayName = R.defaultTo('Имя не указано', user.displayName);
		userClone.emailAddress = R.defaultTo('Email не указан', user.emailAddress);
		return userClone;
	}

	return {
		INIT: INIT,
		sendMsgToCurrentUser: sendMsgToCurrentUser,
		userHandler: userHandler,
		issuesHandler: issuesHandler,
		ticketHandler: ticketHandler,
		sendMsgToUser: sendMsgToUser,
		getUserId: getUserId,
		getParam: getParam,
		getRequestUser: getRequestUser,
		getQueryAssignee: getQueryAssignee,
		createTicketTmpl: createTicketTmpl,
		createUserTmpl: createUserTmpl,
		createIssueTmpl: createIssueTmpl,
		checkTicket: checkTicket,
		checkUser: checkUser,
		errorHandler: errorHandler
	}
}

TJbot().INIT();

module.exports = TJbot;